const path = require("path")
const fs = require("fs")

// -- Webpack configurationuration --

const configuration = {}

// Application entry point
configuration.entry = "./src/server.js"

// We build for node
configuration.target = "node"

// Node module dependencies should not be bundled
configuration.externals = fs.readdirSync("node_modules")
  .reduce(function(acc, mod) {
    if (mod === ".bin") {
      return acc
    }

    acc[mod] = "commonjs " + mod
    return acc
  }, {})

// We are outputting a real node app!
configuration.node = {
  console: false,
  global: false,
  process: false,
  Buffer: false,
  __filename: false,
  __dirname: false,
}

// Output files in the build/ folder
configuration.output = {
  path: path.join(__dirname, "build"),
  filename: "[name].js",
}

configuration.resolve = {
  extensions: [
    ".js",
    ".json",
  ],
}

configuration.module = {}

configuration.module.loaders = [

  // Use babel and eslint to build and validate JavaScript
  {
    test: /\.js$/,
    exclude: /node_modules/,
    loaders: [
      "babel-loader",
    ],
  },

  // Allow loading of JSON files
  {
    test: /\.json$/,
    loader: "json-loader",
  },
]

module.exports = configuration
