# Backend

The backend running a simple GraphQL server. It should serve all the car data.

## Development

In order to setup the development, run

    npm install

Run the following to get the server staring

    npm start

and then browse to [http://localhost:4000/graphiql](http://localhost:4000/graphiql).

### Roadmap

- Add database and population script
- Add JWT
