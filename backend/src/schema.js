import { graphqlExpress } from 'graphql-server-express';
import { makeExecutableSchema } from 'graphql-tools';

import cars from './data/audi_a1.json';

const typeDefs = [`
type Car {
  price: Int
  ageDays: Int
  mileage: Int
  horsepower: Int
}

type Query {
  cars: [Car]
}`];

const resolvers = {
  Query: {
    cars: (root) => {
      return cars;
    }
  }
};

export const schema = makeExecutableSchema({typeDefs, resolvers});

