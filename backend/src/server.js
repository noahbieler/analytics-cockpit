import express from 'express';
import bodyParser from 'body-parser';
import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';
import { schema } from './schema.js';

import jwt from 'express-jwt';

var app = express();

//app.use(jwt({secret: 'secret'})); // TODO with proper encryption
//app.use('/graphql', jwt({
//  secret: 'jwtSecret',
//  credentialsRequired: false,
//  userProperty: 'user',
//}));

app.use('/graphql', bodyParser.json(), graphqlExpress({schema}));
app.use('/graphiql', graphiqlExpress({endpointURL: '/graphql'}));

app.listen(4000, () => console.log('Now browse to http://localhost:4000/graphiql'));
