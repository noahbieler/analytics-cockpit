import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, ButtonToolbar, Button, Panel } from 'react-bootstrap';
import { IndexLinkContainer } from 'react-router-bootstrap';
import Loader from 'react-loader';

import { VictoryScatter, VictoryChart, VictoryAxis, VictoryTheme } from 'victory';

import { fetchData } from '../state/dataLoader';

const DataExplorer = (props) => {
  const chartIsVisible = ((props.data !== undefined && props.data.length > 0) || props.isFetching) ? 'inline' : 'none'
  const loaded = !props.isFetching

  return (
    <Panel 
       header={(<h2>Data Explorer</h2>)}
    >
      <Row><Col xs={12} sm={8} md={8} lg={4} 
       style={{ minHeight: 400 }}>
      <div
        style={{ 
          display: chartIsVisible,
        }}
        >
        <Loader loaded={loaded} >
          <VictoryChart
            padding={{left: 70, top: 20, right: 50, bottom: 50}}
            height={250}
            width={300}
            theme={VictoryTheme.material}
          >
            <VictoryAxis
              tickFormat={(x) => `${x / 1000}k km`}
              style={{ tickLabels: {fontSize: 6}}}
            />
            <VictoryAxis 
              dependentAxis
              tickFormat={(y) => `${y / 1000}k CHF`}
              style={{ tickLabels: {fontSize: 8}}}
            />
            <VictoryScatter
              data={props.data}
              x='mileage'
              y='price'
              style={{
                data: {
                  fill: 'grey',
                  opacity: 0.5,
                },
                labels: {
                  fontSize: 12
                },
              }}
              animate={{
                onEnter: {
                  duration: 500,
                  before: () => ({
                    y: 0
                  })
                }
              }}
            />
          </VictoryChart>
        </Loader>
      </div>
      <ButtonToolbar>
        <Button bsSize='large' onClick={props.loadData}>Load Data</Button>
        <IndexLinkContainer to='/analytics'>
          <Button bsSize='large' bsStyle='primary' disabled={props.data === undefined || props.data.length === 0}>Analyze</Button>
        </IndexLinkContainer>
      </ButtonToolbar>
      </Col></Row>
    </Panel>
  )
}

export default connect(
  state => ({
    data: state.dataPoints.datapoints, 
    isFetching: state.dataPoints.isFetching || false, 
    error: state.dataPoints.error
  }),
  dispatch => ({
    loadData: () => dispatch(fetchData())
  })
)(DataExplorer);
