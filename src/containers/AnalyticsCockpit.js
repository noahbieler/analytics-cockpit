import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, Button, Panel } from 'react-bootstrap';
import { IndexLinkContainer } from 'react-router-bootstrap';

import { VictoryAxis, VictoryChart, VictoryLine, VictoryScatter, VictoryTheme } from 'victory';
import chroma from 'chroma-js';

const colorMap = ({min, max}) => {
  const scale = chroma
      .scale(['yellow', 'red', 'blue'])
      .domain([min, max])
      .mode('lab');
  return (current) => chroma(scale(current))
};

const colorMapFromData = data => {
  let zs = data.map(car => car.horsepower)
  return colorMap({min: Math.min(...zs), max: Math.max(...zs)})
}

const x_range = [
  { x: 0.0 },
  { x: 25000.0 },
  { x: 50000.0 },
  { x: 75000.0 },
  { x: 100000.0 },
  { x: 125000.0 },
  { x: 150000.0 },
  { x: 175000.0 },
  { x: 200000.0 },
  { x: 225000.0 },
  { x: 250000.0 },
  { x: 275000.0 },
  { x: 300000.0 },
]

const fits = [
  {
    color: 'blue',
    c: 60000,
    a: -0.000008
  },
  {
    color: 'red',
    c: 35000,
    a: -0.000008
  },
  {
    color: 'yellow',
    c: 20000,
    a: -0.000008
  },
]

const AnalyticsCockpit = (props) => {
  const myColorMap = colorMapFromData(props.data)

  return (
    <Panel 
       header={(<h2>Analytics Cockpit</h2>)} 
       className="AnalyticsCockpit"
    >
      <Row><Col xs={12} sm={8} md={8} lg={4}>
      <VictoryChart 
        theme={VictoryTheme.material}
        padding={{left: 70, top: 20, right: 50, bottom: 50}}
        height={250}
        width={300}
      >
        <VictoryAxis 
          style={{ tickLabels: {fontSize: 8}}}
          tickFormat={(x) => `${x / 1000}k km`}
        />
        <VictoryAxis 
          dependentAxis
          style={{ tickLabels: {fontSize: 8}}}
          tickFormat={(y) => `${y / 1000}k CHF`}
        />
        <VictoryScatter 
          data={props.data}
          x='mileage'
          y='price'
          style={{
            data: {
              fill: (d) => myColorMap(d.horsepower),
              opacity: 0.5
            }
          }}
        />
        {
          fits.map((fit, index) => 
            <VictoryLine
              key={index}
              data={x_range}
              x='x'
              y={datum => fit.c * Math.exp(fit.a * datum.x)}
              style={{
                data: {
                  stroke: fit.color, 
                  opacity: 0.7,
                }
              }}
              animate={{
                onEnter: {
                  duration: 500,
                  before: () => ({
                    y: 0
                  })
                }
              }}
            />
          )
        }
      </VictoryChart>
      <IndexLinkContainer to='/actions'>
        <Button bsSize='large' bsStyle='primary'>Prepare Mail Campagne</Button>
      </IndexLinkContainer>
      </Col></Row>
    </Panel>
  )
}

export default connect(
  state => ({data: state.dataPoints.datapoints}),
)(AnalyticsCockpit);
