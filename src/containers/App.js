import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Grid, Row, Col } from 'react-bootstrap';

import './App.css';

import NavigationBar from '../views/NavigationBar';
import MainPanel from '../views/MainPanel';
import DataExplorer from './DataExplorer';
import AnalyticsCockpit from './AnalyticsCockpit';
import ActionsCockpit from './ActionsCockpit';
import Login from './Login';

const routes = [
  {
    to: "/",
    title: "Data Explorer",
    exact: true,
    component: DataExplorer
  },
  {
    to: "/analytics",
    title: "Analytics Cockpit",
    exact: false,
    component: AnalyticsCockpit
  },
  {
    to: "/actions",
    title: "Action Cockpit",
    exact: false,
    component: ActionsCockpit
  },
  {
    to: "/login",
    title: "Login",
    exact: false,
    component: Login
  }
];

const App = () => (
  <Router>
    <div className="App">
      <NavigationBar routes={routes} />

      <Grid>
        <Row>
          <Col>
            <MainPanel routes={routes} />
          </Col>
        </Row>
      </Grid>
    </div>
  </Router>
)

export default App;
