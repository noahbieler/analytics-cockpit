import React from 'react';
import { connect } from 'react-redux';
import { Button, Panel, ControlLabel, FormGroup, FormControl } from 'react-bootstrap';

import { setMailText, resetMailText } from '../state/mail';

const ActionCockpit = (props) => {

  const handleChange = (event) => {
    props.setMailText(event.target.value)
  }

  return (
      <Panel header={(<h2>Action Cockpit</h2>)}>
        <FormGroup controlId="mailTemplate">
          <ControlLabel>Mail Template</ControlLabel>
          <FormControl componentClass="textarea" 
                       rows="12" 
                       value={props.mailText}
                       onChange={handleChange}
          />
        </FormGroup>
          <Button bsSize='large' 
                  bsStyle='primary' 
                  disabled={props.mailText === ''} 
                  onClick={props.resetMailText}
          >Send Out</Button>
      </Panel>
    )};

export default connect(
  state => ({mailText: state.mailText}),
  dispatch => ({
    resetMailText: () => dispatch(resetMailText()),
    setMailText: (text) => dispatch(setMailText(text))
  })
)(ActionCockpit);
