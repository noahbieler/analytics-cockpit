import React from 'react';
import { Route } from 'react-router-dom';

const MainPanel = ({routes}) => (
  <div>
    {routes.map((route, index) =>
      <Route exact={route.exact}
             path={route.to}
             component={route.component}
             key={index}/>
    )}
  </div>
)

export default MainPanel;
