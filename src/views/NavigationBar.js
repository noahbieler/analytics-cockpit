import React from 'react';
import { Navbar, Nav, NavItem }  from 'react-bootstrap';
import { IndexLinkContainer } from 'react-router-bootstrap';

const NavigationBar = ({routes}) => (
  <Navbar>
    <Nav>
      {routes.map((route, index) =>
        <IndexLinkContainer to={route.to} key={index}>
          <NavItem>{route.title}</NavItem>
        </IndexLinkContainer>)
      }
    </Nav>
  </Navbar>
)

export default NavigationBar;
