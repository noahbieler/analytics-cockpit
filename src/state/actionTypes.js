/*
 * Define here all action types. This should not leak out of the `stores` 
 * subfolder.
 */

export const RESET_MAIL_TEXT = 'reset_mail_text'
export const SET_MAIL_TEXT = 'set_mail_text'

export const FETCHING_DATA = 'fetching_data'
export const FETCH_DATA_SUCCESS = 'fetch_data_success'
export const FETCH_DATA_ERROR = 'fetch_data_error'

export const SEND_LOGIN = 'send_login'
export const FAILED_LOGIN = 'failed_login'
