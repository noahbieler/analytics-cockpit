import { SET_MAIL_TEXT, RESET_MAIL_TEXT } from './actionTypes'

export const resetMailText = () => ({
  type: RESET_MAIL_TEXT
})

export const setMailText = (text) => ({
  type: SET_MAIL_TEXT,
  text: text
})

const initialState =
`Dear {{user.name}}

We noted that your {{user.car.name}} is priced at {{user.car.price}}.

We looked at the current pricings of all cars similar to yours and found that
yours is priced too {{#user.car.fit.tooHigh}}high{{/user.car.fit.tooHigh}}
{{^user.car.fit.tooHigh}}low{{/user.car.fit.tooHigh}}. We recommend you to
price yours between {{user.car.fit.upper}} and {{user.car.fit.lower}}.

Best wishes,
The Best Car Deal Team
`;

export function handleMailText(state = initialState, action) {
  switch(action.type) {
    case(RESET_MAIL_TEXT):
      return ""
    case(SET_MAIL_TEXT):
      return action.text
    default:
      return state
  }
}
