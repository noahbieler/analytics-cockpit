import { FETCHING_DATA, FETCH_DATA_SUCCESS, FETCH_DATA_ERROR } from './actionTypes'

import DataService from '../service/dataService'

const initialState_ = { isFetching: false, datapoints: [] }

export function handleDataFetch(state = initialState_, action) {
  switch(action.type) {
    case(FETCHING_DATA):
      return { ...state, isFetching: true, error: undefined }
    case(FETCH_DATA_SUCCESS):
      return { ...state, isFetching: false, datapoints: action.data, error: undefined }
    case(FETCH_DATA_ERROR):
      return { ...state, isFetching: false, error: action.error }
    default:
      return state
  }
}

export const fetchingData = () => ({
  type: FETCHING_DATA
})

export const fetchDataError = (error) => ({
  type: FETCH_DATA_ERROR,
  error: error
})

export const fetchDataSuccess = (json) => ({
  type: FETCH_DATA_SUCCESS,
  data: json.cars
})

export const fetchData = () => (dispatch) => {
  dispatch(fetchingData())
  return DataService
    .getData()
    .then(
      response => response.json(),
      error => {
        console.log('An error occured: ', error)
        dispatch(fetchDataError(error))
      }
    )
    .then(json => dispatch(fetchDataSuccess(json.data)))
}
