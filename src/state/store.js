import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'

import { handleDataFetch } from './dataLoader'
import { handleMailText } from './mail'

const rootReducer = combineReducers({
  dataPoints: handleDataFetch,
  mailText: handleMailText
})

const store = createStore(
  rootReducer,
  applyMiddleware(thunkMiddleware)
)

export default store
