import { setMailText, resetMailText, handleMailText } from './mail'

import { SET_MAIL_TEXT, RESET_MAIL_TEXT } from './actionTypes'

it('resets the mail text to empty string', () => {
  const action = resetMailText()
  expect(action.type).toEqual(RESET_MAIL_TEXT)

  const newMailText = handleMailText('test mail', action)
  expect(newMailText).toEqual('')
})

it('sets the mail text', () => {
  const mail = 'new test mail'
  const action = setMailText(mail)
  expect(action.type).toEqual(SET_MAIL_TEXT)

  const newMailText = handleMailText('test mail', action)
  expect(newMailText).toEqual(mail)
})
