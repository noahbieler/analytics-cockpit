import { loadData, handleData, handleDataFetch, fetchingData, fetchDataSuccess, fetchDataError } from './dataLoader'

it('has empty data as initial state', () => {
  const action = {
    type: 'unknown',
    data: [1,2,3]
  }

  const newState = handleDataFetch(undefined, action)
  expect(newState.datapoints).toEqual([])
})

it('indicate fetching when fetching', () => {
  const action = fetchingData()
  const newState = handleDataFetch(undefined, action)
  expect(newState).toEqual({isFetching: true, datapoints:[]})
})

it('return fetched data', () => {
  const cars = [{price: 1}]
  const action = fetchDataSuccess({cars: cars})
  const newState = handleDataFetch({isFetching: true}, action)
  expect(newState).toEqual({isFetching: false, datapoints: cars})
})

it('indicate error', () => {
  const error = "Error message"
  const action = fetchDataError(error)
  const newState = handleDataFetch({isFetching: true, datapoints: []}, action)
  expect(newState).toEqual({isFetching: false, datapoints: [], error: error})
})
