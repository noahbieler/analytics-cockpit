import audis from './audi_a1.json'

const demoMode = false

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export default {
  getData: () => {
    if (demoMode) {
      return sleep(1000).then(s => ({
        json: () => ({
          data: { cars: audis }
        })
      }))
    } else {
      return sleep(1000)
        .then(s => fetch('/graphql?query={cars{price,mileage,horsepower}}'))
    }
  }
}
