# Analytics Cockpit

A sample project that should show a potential analytics cockpit.

## Develpement

To setup all dependencies, run

    npm install

If you are not using the demo mode (`demoMode` in
[`src/service/dataService.js`](src/service/dataService.js)), you have to run
the [backend server](backend/README.md).

To run the app in development, use

    npm start

and open [http://localhost:3000](http://localhost:3000) to view it in the browser.

To run the tests, use

    npm test

To build the app for production to the `build` folder, use

    npm run build

This correctly bundles React in production mode and optimizes the build for the best performance.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
You can find the most recent version of a guide 
[here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Roadmap

 - Add env variable or so for demo mode with json data 
 - Add some loading spinner when analyzing data
 - Refactor out some of the plotting into a `view`
 - Add more tests
